import hashlib
import json
import os
from functools import partial

from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from plasma.models import AbstractModel

from djrichtextfield.models import RichTextField 



def hash_file(file, block_size=65536):
    hasher = hashlib.md5()
    for buf in iter(partial(file.read, block_size), b""):
        hasher.update(buf)
    return hasher.hexdigest()


def upload_to(instance, filename):
    """
    :type instance: dolphin.models.File
    """
    instance.annotation_image.open()
    filename_base, filename_ext = os.path.splitext(filename)

    return "annotate/{}{}".format(hash_file(instance.annotation_image), filename_ext)




class Project(AbstractModel):

    name = models.CharField(_("Name"), max_length=500)
    description = RichTextField()
    collaborators = models.ManyToManyField("accounts.CustomUser", verbose_name=_("Project Collaborators"),related_name ="collaborators")
    collections = models.ManyToManyField("projects.Collection", verbose_name=_("Collections"))
    medias = models.ManyToManyField("projects.Media", verbose_name=_("Medias"))
    is_public = models.BooleanField(_("Is project public"),default=False)
    is_active = models.BooleanField(_("Active or archived"),default=True)
    is_delete = models.BooleanField(_("Soft Delete triggered"),default=False)
    delete_wait_for = models.IntegerField(_("Wait before delete"), default=3)
    created_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Created By"), on_delete=models.CASCADE,related_name="project_created_by")
    updated_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Last modified by"), on_delete=models.CASCADE,related_name="project_updated_by")


    class Meta:
        verbose_name = _("Project")
        verbose_name_plural = _("Projects")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Project_detail", kwargs={"pk": self.pk})


class Invitations(AbstractModel):

    project = models.ForeignKey("projects.Project", verbose_name=_("Project Requested For"), on_delete=models.CASCADE)
    """ 
    INFO: If user and created_by are same, then the user requested to join a group, if they are different, then the created_by requested user to join project
    
    """
    user = models.ForeignKey("accounts.CustomUser", verbose_name=_("User Invited"), on_delete=models.CASCADE, related_name = "user_invited")
    created_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Created By"), on_delete=models.CASCADE,related_name="invitation_created_by")
    last_modified_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Last modified by"), on_delete=models.CASCADE,related_name="invitation_updated_by")



    class Meta:
        verbose_name = _("Invitations")
        verbose_name_plural = _("Invitationss")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Invitations_detail", kwargs={"pk": self.pk})

class Collection(AbstractModel):
    name = models.CharField(_("Name"), max_length=500)
    description = RichTextField(_("Description"), blank=True, null=True)
    is_active = models.BooleanField(_("Is  Actve"), default=True)
    is_public = models.BooleanField(_("Public "), default=True)
    delete_wait_for = models.IntegerField(_("Wait before delete"), default=0)
    created_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Created By"), on_delete=models.CASCADE,related_name="collection_created_by")
    updated_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Last modified by"), on_delete=models.CASCADE,related_name="collection_updated_by")

    def __str__(self):
        return self.name

class Tags(AbstractModel):
    name = models.CharField(_("tag name"), max_length=100)
    count = models.PositiveIntegerField(_("Count of this tag"), default=1)
    color = models.CharField(_("Tag Color hex"), max_length=50,blank=True,null=True)
    
    class Meta:
        verbose_name = _("Tags")
        verbose_name_plural = _("Tags")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Tags_detail", kwargs={"pk": self.pk})

class Media(models.Model):
    media=models.FileField(upload_to='media/')
    orig_name = models.CharField(_("Original Media name"), max_length=500,default="None")
    orig_size = models.PositiveIntegerField(_("File Size"), default=0)
    file_extension = models.CharField(_("File extension"), max_length=50,default="")
    uploaded_date = models.DateTimeField( auto_now_add=True)

    class Meta:
        ordering=['-uploaded_date']

    def __str__(self):
        return str(self.orig_name)
    
class CollectionMediaMapper(AbstractModel):
    media = models.ForeignKey("projects.Media", verbose_name=_("Media Referenced"), on_delete=models.CASCADE)
    collection = models.ForeignKey("projects.Collection", verbose_name=_("Collection"), on_delete=models.CASCADE)
    name = models.CharField(_("Name"), max_length=500)
    description = RichTextField(_("Description"))
    tags = models.ManyToManyField("projects.Tags", verbose_name=_("Annotation Tags"), related_name="media_tags")
    created_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Created By"), on_delete=models.CASCADE,related_name="collection_media_mapper_created_by")
    updated_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Last modified by"), on_delete=models.CASCADE,related_name="collection_media_mapper_updated_by")


class Annotation(AbstractModel):

    media_reference_id = models.ForeignKey("projects.CollectionMediaMapper", verbose_name=_("Media Mapped"), on_delete=models.CASCADE)
    media_target = models.CharField(
        _("Media Target(Time start and end)"), max_length=100
    )

    annotation_text = RichTextField(_("Annotation text"))
    annotation_image = models.ImageField(
        _("Annotation Reference Image"), upload_to=upload_to, blank=True, null=True
    )
    # tags = models.ManyToManyField("common.Tags", verbose_name=_("tags"))
    is_public = models.BooleanField(_("Public"), default=True)
    is_delete = models.BooleanField(_("Soft Deleted ?"), default=False)
    is_instance_admin_withheld = models.BooleanField(
        _("withheld by instance admin?"), default=False
    )
    is_project_admin_withheld = models.BooleanField(
        _("withheld by project admin?"), default=False
    )
    tags = models.ManyToManyField("projects.Tags", verbose_name=_("Annotation Tags"), related_name="annotation_tags")
    created_by = models.ForeignKey(
        "accounts.CustomUser",
        verbose_name=_("Who created the annotation"),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name = "annotation_created_by"
    )
    updated_by = models.ForeignKey("accounts.CustomUser", verbose_name=_("Last modified by"), on_delete=models.CASCADE,related_name="annotation_updated_by")
    

    class Meta:
        verbose_name = _("Annotation")
        verbose_name_plural = _("Annotations")
        ordering = ["updated_at"]

    def __str__(self):
        return self.media_reference_id

    def get_absolute_url(self):
        return reverse("Annotation_detail", kwargs={"pk": self.pk})

    def annotation_structure(self, media_id):
        "Returns every object in annotation structure"
        data = Annotation.objects.filter(media_reference_id=media_id)

        resp_data = []
        resp = {}  # This is the final response
        for d in data:
            ref_json = {}  # Referece json
            a_struct = {}  # Annotation response json

            # This ensures we define the structure of the sample json as per our annotation structure
            with open("./papadapi/annotate/annotation_structure.json") as f:
                ref_json = json.loads(f.read())

            a_struct = ref_json
            a_struct["id"] = d.uuid
            a_struct["created"] = d.created_at
            a_struct["modified"] = d.updated_at
            a_struct["target"]["id"] = d.media_reference_id
            a_struct["target"]["selector"]["value"] = d.media_target
            a_struct["body"][0]["id"] = d.id  # id, value, created
            tags = ""
            for tag in d.tags.all():
                tags = tags + "," + tag.name
            a_struct["body"][0]["value"] = tags
            a_struct["body"][0]["created"] = d.created_at

            a_struct["body"][1]["items"][0]["id"] = d.id
            a_struct["body"][1]["items"][0]["value"] = d.annotation_text
            a_struct["body"][1]["items"][0]["created"] = d.created_at
            if d.annotation_image:
                a_struct["body"][1]["items"].append({})
                a_struct["body"][1]["items"][1]["id"] = d.id
                a_struct["body"][1]["items"][1]["type"] = "Image"
                a_struct["body"][1]["items"][1]["value"] = d.annotation_image.url
                a_struct["body"][1]["items"][1]["created"] = d.created_at
            resp_data.append(a_struct)
        resp["count"] = data.count()
        resp["prev"] = "null"
        resp["next"] = "null"
        resp["results"] = resp_data
        return resp

