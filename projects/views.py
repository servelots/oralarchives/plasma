import os 

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import reverse
from django.db.models import Count, Q, F, Value
from django.shortcuts import get_list_or_404, get_object_or_404
from django.http import JsonResponse, HttpResponse
# Import Models 
from projects.models import Project, Collection, Media, CollectionMediaMapper, Annotation, Tags

#Import Forms
from projects.forms import ProjectForm, CollectionForm, CollectionMediaMapperForm


@login_required
def dashboard(request):
    projects = Project.objects.filter(Q(is_public=True, is_active=True) | Q(collaborators__in=[request.user]))
    return render(request,'projects/dashboard.html',{'projects':projects})

@login_required
def manage_project(request,project_id=None):
    if request.method == "POST":
        data = request.POST
        name = data["name"]
        description = data["description"]
        is_public = True if "is_public" in data else False
        delete_wait_for = data["delete_wait_for"]

        project = Project.objects.create(
                            name=name,
                            description=description,
                            is_public=is_public,
                            is_active = True,
                            created_by = request.user,
                            updated_by = request.user,
                            delete_wait_for = delete_wait_for,
                            )
        project.collaborators.add(request.user)
        project.save()

        return redirect(reverse('dashboard'))
    else:
        project_form = ProjectForm()
        return render(request,"projects/create_project.html",{"project_form":project_form})
        
@login_required
def get_project_details(request,project_id):
    project = get_object_or_404(Project, uuid=project_id)
    return render(request,"projects/project_details.html",{'project':project})
        
        
@login_required
def manage_collection(request,project_id=None):
    if request.method == "POST":
        project = get_object_or_404(Project,uuid=project_id)
        data = request.POST
        name = data["name"]
        description = data["description"]
        is_public = True if "is_public" in data else False
        delete_wait_for = data["delete_wait_for"]

        collection = Collection.objects.create(
                            name=name,
                            description=description,
                            is_public=is_public,
                            is_active = True,
                            created_by = request.user,
                            updated_by = request.user,
                            delete_wait_for = delete_wait_for,
                            )
        collection.save()
        project.collections.add(collection)
        return redirect(reverse('project_details',kwargs={'project_id':project_id}))
    else:
        collection_form = CollectionForm()
        return render(request,"projects/create_collection.html",{"collection_form":collection_form})


@login_required
def get_collection_details(request,project_id, collection_id):
    project = get_object_or_404(Project, uuid=project_id)
    collection = get_object_or_404(Collection,uuid = collection_id)
    if collection in project.collections.all():
        media_in_collection = CollectionMediaMapper.objects.filter(collection=collection)
        return render(request,"projects/collection_details.html",{'project':project, "collection":collection,"media_in_collection":media_in_collection})
    else:
        return HttpResponse("Collection and Project mismatch")        

@login_required
def map_media_to_collection(request,project_id=None,collection_id=None):
    if request.method == "POST":
        project = get_object_or_404(Project,uuid=project_id)
        collection = get_object_or_404(Collection,uuid = collection_id)
        if collection in project.collections.all():
            data = request.POST 
            name = data.get("name")
            description = data.get("description")
            media = get_object_or_404(Media,id=data.get("media"))
            media_mapper = CollectionMediaMapper.objects.create(
                name = name,
                description = description,
                collection = collection,
                created_by = request.user,
                updated_by = request.user,
                media = media
            )
            return redirect(reverse('collection_details',kwargs = {"project_id":project.uuid,"collection_id":collection.uuid}))
        else:
            return redirect(reverse('collection_details',kwargs = {"project_id":project.uuid,"collection_id":collection.uuid}))
    else:
        map_collection_form = CollectionMediaMapperForm(project_id=project_id)
        return render(request, "projects/map_media_to_collection.html",{"project":project_id, "collection":collection_id,"map_collection_form":map_collection_form})

def view_media(request, project_id, collection_id, media_mapper_id):
    media = get_object_or_404(CollectionMediaMapper,uuid=media_mapper_id)
    project = get_object_or_404(Project,uuid=project_id)
    collection = get_object_or_404(Collection,uuid = collection_id)
    # project_collections = project.collections.all()
    # tags_collection = CollectionMediaMapper.objects.filter(collection__in=project_collections).values("tags")
    # print(tags_collection)
    annotations = Annotation.objects.filter(media_reference_id = media)
    tags = Tags.objects.all()
    return render(request,"projects/view_media.html",{"project":project, "collection":collection, "media":media,"annotations":annotations,"tags":tags})

@login_required
def file_upload(request, project_id=None):
    if request.method == 'POST':
        project = get_object_or_404(Project,uuid=project_id)
        my_file=request.FILES.get('file')
        print(my_file.size)
        media = Media.objects.create(media=my_file,orig_name=my_file.name,file_extension=my_file.content_type, orig_size=my_file.size)
        project.medias.add(media)
        return redirect(reverse('project_details',kwargs={'project_id':project_id}))
    return JsonResponse({'post':'fasle'})

@login_required
def create_annotation(request,project_id=None, collection_id=None, media_mapper_id=None):
    project = get_object_or_404(Project,uuid=project_id)
    collection = get_object_or_404(Collection,uuid = collection_id)
    media_mapper = get_object_or_404(CollectionMediaMapper, uuid=media_mapper_id)
    data = request.POST
    media_target = data.get("annotation_start_time")+","+data.get("annotation_end_time")
    annotation_text = data.get("annotation_text")
    tags = data.getlist("tags")
    
    annotation = Annotation.objects.create(
        media_reference_id = media_mapper,
        media_target = media_target,
        annotation_text = annotation_text,
        created_by = request.user,
        updated_by = request.user,
    )
    for tag in tags:
        t,created = Tags.objects.get_or_create(name=tag.lower())
        annotation.tags.add(t)
    response = render(request, 'projects/_includes/annotation_form.html', {})
    response['HX-Trigger'] = 'newAnnotationAdded'
    return response

@login_required
def list_annotation(request,project_id=None, collection_id=None, media_mapper_id=None):
    media_mapper = get_object_or_404(CollectionMediaMapper, uuid=media_mapper_id)
    annotations = Annotation.objects.filter(media_reference_id=media_mapper).order_by(F('created_at').desc(nulls_last=True))
    return render(request,"projects/_includes/annotation_list.html",{"annotations":annotations})