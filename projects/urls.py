from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from projects.views import dashboard, manage_project,get_project_details
from projects.views import manage_collection, map_media_to_collection, get_collection_details
from projects.views import file_upload, view_media, create_annotation, list_annotation

urlpatterns = [
    path("",dashboard,name="dashboard"),
    path("manage_project/",manage_project,name="create_project"),
    path("project/<uuid:project_id>/",get_project_details,name="project_details"),
    path("manage_collection/<uuid:project_id>/",manage_collection,name="create_collection"),
    path("project/<uuid:project_id>/collection/<uuid:collection_id>/",get_collection_details,name="collection_details"),
    path("project/<uuid:project_id>/collection/<uuid:collection_id>/map_media/",map_media_to_collection,name="map_media_to_collection"),
    path("project/<uuid:project_id>/collection/<uuid:collection_id>/media/<uuid:media_mapper_id>/",view_media,name="view_media"),
    path('project/<uuid:project_id>/mediaupload/', file_upload,name="media_upload"),    
    path('project/<uuid:project_id>/collection/<uuid:collection_id>/create_annotation/<uuid:media_mapper_id>/', create_annotation, name="create_annotation"),
    path('project/<uuid:project_id>/collection/<uuid:collection_id>/list_annotation/<uuid:media_mapper_id>/', list_annotation, name="list_annotations")
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)