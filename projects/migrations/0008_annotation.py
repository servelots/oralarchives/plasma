# Generated by Django 4.2 on 2023-07-14 04:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import djrichtextfield.models
import plasma.models
import projects.models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projects', '0007_alter_media_options_rename_date_media_uploaded_date_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Annotation',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', plasma.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='UUID')),
                ('media_reference_id', models.URLField(max_length=500, verbose_name='Media Reference URL')),
                ('media_target', models.CharField(max_length=100, verbose_name='Media Target(Time start and end)')),
                ('annotation_text', djrichtextfield.models.RichTextField(verbose_name='Annotation text')),
                ('annotation_image', models.ImageField(blank=True, null=True, upload_to=projects.models.upload_to, verbose_name='Annotation Reference Image')),
                ('is_public', models.BooleanField(default=True, verbose_name='Public')),
                ('is_delete', models.BooleanField(default=False, verbose_name='Soft Deleted ?')),
                ('is_instance_admin_withheld', models.BooleanField(default=False, verbose_name='withheld by instance admin?')),
                ('is_project_admin_withheld', models.BooleanField(default=False, verbose_name='withheld by project admin?')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='annotation_created_by', to=settings.AUTH_USER_MODEL, verbose_name='Who created the annotation')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='annotation_updated_by', to=settings.AUTH_USER_MODEL, verbose_name='Last modified by')),
            ],
            options={
                'verbose_name': 'Annotation',
                'verbose_name_plural': 'Annotations',
                'ordering': ['updated_at'],
            },
        ),
    ]
