# Generated by Django 4.2 on 2023-07-11 05:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import djrichtextfield.models
import plasma.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Tags',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', plasma.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=100, verbose_name='tag name')),
                ('count', models.PositiveIntegerField(default=1, verbose_name='Count of this tag')),
                ('color', models.CharField(max_length=50, verbose_name='Tag Color hex')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tags_created_by', to=settings.AUTH_USER_MODEL, verbose_name='Created By')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tags_updated_by', to=settings.AUTH_USER_MODEL, verbose_name='Last modified by')),
            ],
            options={
                'verbose_name': 'Tags',
                'verbose_name_plural': 'Tags',
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', plasma.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=500, verbose_name='Name')),
                ('description', djrichtextfield.models.RichTextField()),
                ('is_public', models.BooleanField(default=False, verbose_name='Is project public')),
                ('is_active', models.BooleanField(default=True, verbose_name='Active or archived')),
                ('is_delete', models.BooleanField(default=False, verbose_name='Soft Delete triggered')),
                ('delete_wait_for', models.IntegerField(default=3, verbose_name='Wait before delete')),
                ('collaborators', models.ManyToManyField(related_name='collaborators', to=settings.AUTH_USER_MODEL, verbose_name='Project Collaborators')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project_created_by', to=settings.AUTH_USER_MODEL, verbose_name='Created By')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project_updated_by', to=settings.AUTH_USER_MODEL, verbose_name='Last modified by')),
            ],
            options={
                'verbose_name': 'Project',
                'verbose_name_plural': 'Projects',
            },
        ),
        migrations.CreateModel(
            name='Invitations',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', plasma.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(primary_key=True, serialize=False, verbose_name='UUID')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='invitation_created_by', to=settings.AUTH_USER_MODEL, verbose_name='Created By')),
                ('last_modified_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='invitation_updated_by', to=settings.AUTH_USER_MODEL, verbose_name='Last modified by')),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.project', verbose_name='Project Requested For')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_invited', to=settings.AUTH_USER_MODEL, verbose_name='User Invited')),
            ],
            options={
                'verbose_name': 'Invitations',
                'verbose_name_plural': 'Invitationss',
            },
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', plasma.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=500, verbose_name='Name')),
                ('description', djrichtextfield.models.RichTextField(blank=True, null=True, verbose_name='Description')),
                ('is_active', models.BooleanField(default=True, verbose_name='Is  Actve')),
                ('is_public', models.BooleanField(default=True, verbose_name='Public ')),
                ('delete_wait_for', models.IntegerField(default=0, verbose_name='Wait before delete')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='collection_created_by', to=settings.AUTH_USER_MODEL, verbose_name='Created By')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='collection_updated_by', to=settings.AUTH_USER_MODEL, verbose_name='Last modified by')),
            ],
            options={
                'verbose_name': 'AbstractModel',
                'verbose_name_plural': 'AbstractModels',
                'abstract': False,
            },
        ),
    ]
