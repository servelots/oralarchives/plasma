# Generated by Django 4.2 on 2023-07-11 20:33

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import djrichtextfield.models
import plasma.models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projects', '0005_project_medias'),
    ]

    operations = [
        migrations.CreateModel(
            name='CollectionMediaMapper',
            fields=[
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created At')),
                ('updated_at', plasma.models.AutoDateTimeField(default=django.utils.timezone.now, verbose_name='Updated At')),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='UUID')),
                ('name', models.CharField(max_length=500, verbose_name='Name')),
                ('description', djrichtextfield.models.RichTextField(verbose_name='Description')),
                ('collection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.collection', verbose_name='Collection')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='collection_media_mapper_created_by', to=settings.AUTH_USER_MODEL, verbose_name='Created By')),
                ('media', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.media', verbose_name='Media Referenced')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='collection_media_mapper_updated_by', to=settings.AUTH_USER_MODEL, verbose_name='Last modified by')),
            ],
            options={
                'verbose_name': 'AbstractModel',
                'verbose_name_plural': 'AbstractModels',
                'abstract': False,
            },
        ),
    ]
