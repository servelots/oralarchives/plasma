from django import forms

from projects.models import Project, Collection, CollectionMediaMapper
from djrichtextfield.widgets import RichTextWidget

class ProjectForm(forms.ModelForm):
    
    class Meta:
        model = Project
        fields = ("name","description","is_public","delete_wait_for")

class CollectionForm(forms.ModelForm):
    
    description = forms.CharField(widget=RichTextWidget())
    class Meta:
        model = Collection
        fields = ("name","description","is_public","delete_wait_for")

class FileFieldForm(forms.Form):
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    
class CollectionMediaMapperForm(forms.ModelForm):
    
    class Meta:
        model = CollectionMediaMapper
        fields = ("media","name","description")
        
    def __init__(self,project_id,*args,**kwargs):
        super (CollectionMediaMapperForm,self ).__init__(*args,**kwargs) # populates the post
        self.fields['media'].queryset = Project.objects.get(uuid=project_id).medias

