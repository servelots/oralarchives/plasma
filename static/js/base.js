$('#tag_selector').select2({
    closeOnSelect: false,
    multiple: true,
    tags: true,
    tokenSeparators: [",",],
    createTag: function (tag) {
        return {
            id: tag.term,
            text: tag.term,
            // add indicator:
            isNew : true
        };
    }
}).on("select2:select", function(e) {
    if(e.params.data.isNew){
        // append the new option element prenamently:
        $(this).find('[value="'+e.params.data.id+'"]').replaceWith('<option selected value="'+e.params.data.id+'">'+e.params.data.text+'</option>');
        // store the new tag and use $.ajax if needed here. Avoiding doing it as the Annotate button is a better user validation for a tag creation than a possibly typo tag 
        
    }
});